# 'User Service' Rest Service

`user-service` is a REST microservice application.

## Implementation

Following tech spec is used for the TDD based implementation.

- *Java*
- *Spring Boot*
- *maven*
- *JUnit*
- *h2*

The project is organized as a *maven* project and in order to compile, test and create a package *maven* is used.

### Building the application

You could use maven to test and build the jar file.

* In the root directory of the project run the following commands

```bash
# Compile
mvn -B clean compile

#Test
mvn -B clean test


#Create the package
mvn -B clean package

```

## Using the API

### Running the application

* Start the application with the following command:

```bash

#Under the root folder of the project

java -jar target/user-service-0.0.1.jar

```


### Request

The endpoint of the application as given in the following table.


|End Point                                                     | Operation    |Port  |
|--------------------------------------------------------------|--------------|------|
|http://localhost:8080/api/v1/users                            |POST          | 8080 |
|http://localhost:8080/api/v1/users                            |GET           | 8080 |
|http://localhost:8080/api/v1/users/{name}                     |GET           | 8080 |


* Sample Transaction Request for Post
```json

{
  "name" : "name",
  "sex" : "male",
  "age" : 30,
  "country": "UK"
}

```

## Database

TABLE NAME : USERDATA

 |Column Name      | Type                | Not Null |
 |-----------------|---------------------|----------|
 |NAME             | VARCHAR             | Y        |
 |AGE              | INTEGER             | Y        |
 |SEX              | VARCHAR             | Y        |
 |COUNTRY          | VARCHAR             | Y        |
 |DATECREATED      | TIMESTAMP           | Y        |