package co.uk.sc.service;

import co.uk.sc.dto.UserDto;
import co.uk.sc.entity.User;
import co.uk.sc.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository repository;

    public UserServiceImpl(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public void saveUser(UserDto dto) {

        User user = User.builder()
                .name(dto.getName())
                .sex(dto.getSex())
                .age(dto.getAge())
                .country(dto.getCountry())
                .dateCreated(LocalDateTime.now())
                .build();

        repository.save(user);
    }

    @Override
    public List<User> getAllUser() {

        return repository.findAll();
    }

    @Override
    public User getUser(String name) {

        return repository.findById(name).orElse(User.builder().build());
    }
}
