package co.uk.sc.service;

import co.uk.sc.dto.UserDto;
import co.uk.sc.entity.User;

import java.util.List;

public interface UserService {
    void saveUser(UserDto dto);
    List<User>  getAllUser();
    User getUser(String name);
}
