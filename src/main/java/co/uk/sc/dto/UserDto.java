package co.uk.sc.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Builder
@Data
public class UserDto {
    @NotNull(message = "Name may not be null")
    private String name;

    @NotNull(message = "Sex may not be null")
    private Sex sex;

    @NotNull(message = "Age may not be null")
    private Integer age;

    @NotNull(message = "Country may not be null")
    private String country;
}

