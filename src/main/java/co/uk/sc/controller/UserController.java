package co.uk.sc.controller;

import co.uk.sc.dto.UserDto;
import co.uk.sc.entity.User;
import co.uk.sc.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {
    private final UserService service;

    public UserController(UserService service) {
        this.service = service;
    }

    @PostMapping()
    public ResponseEntity save(@Valid @RequestBody UserDto dto) {
        service.saveUser(dto);
        return  ResponseEntity.status(HttpStatus.CREATED).build();
    }


    @GetMapping(value = "/{name}")
    public ResponseEntity<User> get(@PathVariable String name) {
        return  ResponseEntity.ok(service.getUser(name));
    }

    @GetMapping()
    public ResponseEntity<List<User>> getAll() {
        return  ResponseEntity.ok(service.getAllUser());
    }
}
