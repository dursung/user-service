package co.uk.sc.entity;

import co.uk.sc.dto.Sex;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name="USERDATA")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class User {

    @Id
    @Column(name= "name")
    private String name;

    @Column(name="sex")
    private Sex sex;

    @Column(name="age")
    private Integer age;

    @Column(name="country")
    private String country;

    @Column(name="dateCreated")
    private LocalDateTime dateCreated;
}
