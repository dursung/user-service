package co.uk.sc.controller;

import co.uk.sc.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;


@WebMvcTest(controllers = UserController.class)
@AutoConfigureWebClient
class UserControllerIntegrationTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private UserService service;
    private ObjectMapper objectMapper;

    @BeforeEach
    public void setUp() {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
        objectMapper = new ObjectMapper();
    }

    @Test
    public void givenEmptyDto_whenCallSave_thenReturnsClientError() throws Exception {

        mockMvc.perform(post("/api/v1/users")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(status().is4xxClientError());
    }


    @Test
    public void givenInvalidDto_whenCallSave_thenReturnsClientError() throws Exception {
        Resource resource = new ClassPathResource("/fixtures/invalid_request.json");

        mockMvc.perform(post("/api/v1/users")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(IOUtils.toString(resource.getInputStream(), "UTF-8")
                )).andExpect(status().is4xxClientError());
    }


    @Test
    public void givenDto_whenCallSave_thenReturnsCreated() throws Exception {
        Resource resource = new ClassPathResource("/fixtures/request.json");

        mockMvc.perform(post("/api/v1/users")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(IOUtils.toString(resource.getInputStream(), "UTF-8")
                )).andExpect(status().isCreated());
    }

    @Test
    public void givenName_whenCallGet_thenReturnsOk() throws Exception {

        mockMvc.perform(get("/api/v1/users/"+"name")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(status().isOk());
    }

    @Test
    public void given_whenCallGetAll_thenReturnsOk() throws Exception {

        mockMvc.perform(get("/api/v1/users")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk());
    }

}