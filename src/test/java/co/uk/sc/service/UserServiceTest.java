package co.uk.sc.service;

import co.uk.sc.dto.Sex;
import co.uk.sc.dto.UserDto;
import co.uk.sc.entity.User;
import co.uk.sc.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class UserServiceTest {

    private UserRepository repository;
    private UserService service;

    @BeforeEach
    void setUp() {
        repository = mock(UserRepository.class);
        service = new UserServiceImpl(repository);
    }

    @Test
    void givenDto_whenCallSave_thenCallRepositorySave() {

        service.saveUser(UserDto.builder().name("name").sex(Sex.Male).age(30).country("UK").build());

        verify(repository).save(any());
    }

    @Test
    void givenName_whenCallGetUser_thenReturnUser() {

        when(repository.findById("name"))
                .thenReturn(Optional.of(User.builder().name("name").age(30).country("UK").sex(Sex.Male).build()));

        User user = service.getUser("name");

        assertEquals(user.getAge(),30);
        assertEquals(user.getCountry(),"UK");
        assertEquals(user.getName(),"name");
        assertEquals(user.getSex(),Sex.Male);
    }

    @Test
    void givenEmptyInput_whenCallGetAllUser_thenReturnAllUsers() {
        when(repository.findAll())
                .thenReturn(Collections.singletonList(
                        User.builder().name("name").age(30).country("UK").sex(Sex.Male).build()));

        List<User> user = service.getAllUser();

        assertEquals(user.get(0).getAge(),30);
        assertEquals(user.get(0).getCountry(),"UK");
        assertEquals(user.get(0).getName(),"name");
        assertEquals(user.get(0).getSex(),Sex.Male);
    }
}