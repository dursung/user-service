package co.uk.sc.controller;

import co.uk.sc.dto.UserDto;
import co.uk.sc.entity.User;
import co.uk.sc.service.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class UserControllerTest {
    private UserController controller;
    private UserService service;

    @BeforeEach
    public void setUp() {
        service = mock(UserService.class);
        controller = new UserController(service);
    }

    @Test
    public void givenDto_whenCallSave_thenReturnCREATED(){
        ResponseEntity response = controller.save(UserDto.builder().build());
        verify(service).saveUser(any());
        Assertions.assertEquals(response.getStatusCode(), HttpStatus.CREATED);
    }

    @Test
    public void givenName_whenCallGet_thenReturnOK(){
        ResponseEntity response = controller.get("Name");
        verify(service).getUser("Name");
        when(service.getUser("Name")).thenReturn(User.builder().name("Name").build());
        Assertions.assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void givenEmpty_whenCallGetAll_thenReturnOK(){
        ResponseEntity response = controller.getAll();
        verify(service).getAllUser();
        when(service.getAllUser()).thenReturn(Collections.singletonList(User.builder().name("Name").build()));
        Assertions.assertEquals(response.getStatusCode(), HttpStatus.OK);
    }
}